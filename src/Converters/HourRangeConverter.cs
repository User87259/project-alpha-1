using spothero_project.Models;
using System;
using System.Linq;

namespace spothero_project.Converters
{
    public class HourRangeConverter : IHourRangeConverter
    {
        public HourRange ToHourRange(string hourRangeStr)
        {
            var hours = hourRangeStr.Split('-')
                    .Select(v => MilitaryHourStringToInt(v.Trim()))
                    .ToList();

            if (hours.Count != 2)
                throw new InvalidOperationException("A time range must be two hour strings separated by a '-'");

            return new HourRange() { StartHour = hours[0], EndHour = hours[1] };
        }

        private int MilitaryHourStringToInt(string militaryHour)
            => int.Parse(militaryHour) / 100;
    }
}