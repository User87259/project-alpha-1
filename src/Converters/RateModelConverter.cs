using spothero_project.Models;

namespace spothero_project.Converters
{
    public class RateModelConverter : IRateModelConverter
    {
        private readonly IHourRangeConverter _hourRangeConverter;
        private readonly IDayConverter _dayConverter;

        public RateModelConverter(IHourRangeConverter hourRangeConverter, IDayConverter dayConverter)
        {
            _hourRangeConverter = hourRangeConverter;
            _dayConverter = dayConverter;
        }

        public RateModel ToRateModel(RawRateModel rawRateModel)
        {
            return new RateModel(_dayConverter.ToDays(rawRateModel.Days), _hourRangeConverter.ToHourRange(rawRateModel.Times), rawRateModel.Price);
        }
    }
}