using spothero_project.Models;

namespace spothero_project.Converters
{
    public interface IHourRangeConverter
    {
        HourRange ToHourRange(string hourRangeStr);
    }
}