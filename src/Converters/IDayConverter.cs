using System.Collections.Generic;
using spothero_project.Models;
using System;

namespace spothero_project.Converters
{
    public interface IDayConverter
    {
        List<DayOfWeek> ToDays(string daysStr);
    }
}