using System.Collections.Generic;
using System.Linq;
using System;
using spothero_project.Models;

namespace spothero_project.Converters
{
    public class DayConverter : IDayConverter
    {
        private static readonly Dictionary<string, DayOfWeek> _dayNameToEnum;

        static DayConverter()
        {
            _dayNameToEnum = Enum.GetValues(typeof(DayOfWeek))
                                 .Cast<DayOfWeek>()
                                 .ToDictionary(x => x.ToString(), x => x);
        }

        public List<DayOfWeek> ToDays(string daysStr)
            =>  daysStr.Split(',').Select(d => ToDay(d.Trim())).ToList();

        private DayOfWeek ToDay(string dayStr)
            => _dayNameToEnum.Single(p => p.Key.StartsWith(dayStr, StringComparison.OrdinalIgnoreCase)).Value;
    }
}