using spothero_project.Models;
using System.Linq;

namespace spothero_project.Converters
{
    public class RatesModelConverter : IRatesModelConverter
    {
        private readonly IRateModelConverter _rateModelConveter;

        public RatesModelConverter(IRateModelConverter rateModelConverter)
        {
            _rateModelConveter = rateModelConverter;
        }

        public RatesModel ToRatesModel(RawRatesModel rawRatesModel)
        {
            return new RatesModel(rawRatesModel.Rates.Select(r => _rateModelConveter.ToRateModel(r)).ToList());
        }
    }
}