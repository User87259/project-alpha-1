using spothero_project.Models;

namespace spothero_project.Converters
{
    public interface IRatesModelConverter
    {
        RatesModel ToRatesModel(RawRatesModel rawRatesModel);
    }
}