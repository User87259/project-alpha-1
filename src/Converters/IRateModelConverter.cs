using spothero_project.Models;

namespace spothero_project.Converters
{
    public interface IRateModelConverter
    {
        RateModel ToRateModel(RawRateModel rawRateModel);
    }
}