﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.IO;
using spothero_project.Services;
using spothero_project.ModelBinders;
using spothero_project.ActionFilters;
using spothero_project.Models;
using spothero_project.Converters;
using spothero_project.Swagger.OperationFilters;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using spothero_project.Utils;

namespace spothero_project
{
    public class Startup
    {
        public const string INTEGRATION_TESTING_ENV_NAME = "IntegreationTesting";

        private readonly IHostingEnvironment _environment;
        private readonly IConfiguration _configuration;
        private readonly IFileProvider _integrationTestRatesFileProvider;

        public Startup(IHostingEnvironment environment, IFileProvider integrationTestRatesFileProvider = null)
        {
            _environment = environment;
            _integrationTestRatesFileProvider = integrationTestRatesFileProvider;

            var builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            _configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(new CommonActionFilter());
                options.ModelBinderProviders.Insert(0, new RequestedTimeRangeBinderProvider());
            });

            //On other projects I have worked on Swagger doesn't have a problem running in an integration test, but for some reason this
            //project won't deploy the Swagger xml to the test directory so it is easier just to turn off Swagger during integration testing.
            if (_environment.EnvironmentName != INTEGRATION_TESTING_ENV_NAME)
            {
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title = "Spothero Project", Version = "v1" });
                    
                    var xmlPath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Spothero_Project.xml");
                    c.IncludeXmlComments(xmlPath);

                    c.OperationFilter<RequestedTimeRangeFilter>();
                    c.OperationFilter<ContentTypeOperationFilter>();
                    c.OperationFilter<BadRequestOperationFilter>();
                });
            }

            //If an integration test file provider isn't specified then a real one should be used
            var ratesFileProvider = _integrationTestRatesFileProvider ?? new PhysicalFileProvider(PlatformServices.Default.Application.ApplicationBasePath);
            var json = FileProviderUtils.GetFileContents(ratesFileProvider, "DeploymentFiles/rates.json");

            var rawRatesModel = JsonConvert.DeserializeObject<RawRatesModel>(json);

            //Convert from the RawRatesModel before it is put into the ServiceCollection so that it is only done during application
            //startup rather than perhaps for every request. Further work could be done to set a watch on the IFileProvider that
            //represents so that the application dynamically updates to reflect changes in the json data, but this is out of scope for this project....
            var ratesModel = ConvertToRatesModel(rawRatesModel);

            services.AddTransient<IRatesModel>(sp => ratesModel);
            services.AddTransient<IRateProvider, RateProvider>();
            services.AddTransient<IResponseFormatter, ResponseFormatter>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            if (_environment.EnvironmentName != INTEGRATION_TESTING_ENV_NAME)
                app.UseSwagger();

            app.UseMvc();
            
            if (_environment.EnvironmentName != INTEGRATION_TESTING_ENV_NAME)
                app.UseSwaggerUI(opt => { opt.SwaggerEndpoint("/swagger/v1/swagger.json", "Spothero Project v1"); });
        }

        private RatesModel ConvertToRatesModel(RawRatesModel rawRatesModel)
        {
            ServiceCollection sc = new ServiceCollection();
            
            sc.AddTransient<IRatesModelConverter, RatesModelConverter>();
            sc.AddTransient<IRateModelConverter, RateModelConverter>();
            sc.AddTransient<IHourRangeConverter, HourRangeConverter>();
            sc.AddTransient<IDayConverter, DayConverter>();

            var provider = sc.BuildServiceProvider();
            return provider.GetService<IRatesModelConverter>().ToRatesModel(rawRatesModel);
        }
    }
}
