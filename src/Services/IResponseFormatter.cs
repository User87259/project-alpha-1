using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace spothero_project.Services
{
    public interface IResponseFormatter
    {
        IActionResult Format(object responseContent, IHeaderDictionary requestHeaders);
    }
}