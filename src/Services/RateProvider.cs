using System;
using spothero_project.Models;
using System.Linq;

namespace spothero_project.Services
{
    public class RateProvider : IRateProvider
    {
        private readonly IRatesModel _ratesModel;

        public RateProvider(IRatesModel ratesModel)
        {
            _ratesModel = ratesModel;
        }

        public int? GetRate(RequestedTimeRange requestedTimeRange)
            => _ratesModel.Rates.FirstOrDefault(r => r.Days.Contains(requestedTimeRange.Day) && RangeContains(r.Hours, requestedTimeRange.Hours))?.Price;

        private bool RangeContains(HourRange firstRange, HourRange secondRange)
            => firstRange.StartHour < secondRange.StartHour && firstRange.EndHour > secondRange.EndHour;
    }
}