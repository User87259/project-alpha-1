using spothero_project.Models;

namespace spothero_project.Services
{
    public interface IRateProvider
    {
        int? GetRate(RequestedTimeRange requestedTimeRange);
    }
}