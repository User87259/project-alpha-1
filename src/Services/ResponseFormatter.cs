using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Primitives;
using spothero_project.CustomActionResults;

namespace spothero_project.Services
{
    public class ResponseFormatter : IResponseFormatter
    {
        private static readonly Regex _xmlMimeTypesRegex = new Regex("(application/xml)|(text/xml)", RegexOptions.IgnoreCase);

        public IActionResult Format(object responseContent, IHeaderDictionary requestHeaders)
        {
            //Hack - Unfortunately the easy way of just adding an XML formatted to the Mvc options in startup does not work in this project.
            //There is a lot of complaining about it online https://stackoverflow.com/questions/42653698/aspnetcore-api-content-negotiation-not-working.
            //None of the "solutions" that I tried actually worked in this .net core 2.0 project so I am just doing it manually.

            if (!requestHeaders.TryGetValue("Accept", out StringValues acceptHeaderValue) || _xmlMimeTypesRegex.IsMatch(acceptHeaderValue[0]))
                return new XmlContentResult(responseContent);
            else
                return new JsonResult(responseContent);
        }
    }
}