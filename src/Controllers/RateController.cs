﻿using Microsoft.AspNetCore.Mvc;
using spothero_project.Services;
using spothero_project.Models;
using System;
using spothero_project.Swagger.CustomAttributes;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace spothero_project.Controllers
{
    [Route("api/[controller]")]
    public class RateController : Controller
    {
        private readonly IRateProvider _rateProvider;
        private readonly IResponseFormatter _responseFormatter;

        public RateController(IRateProvider rateProvider, IResponseFormatter responseFormatter)
        {
            _rateProvider = rateProvider;
            _responseFormatter = responseFormatter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(RateInfo), 200)]
        [ProducesResponseType(typeof(void), 404)]
        [SwaggerResponseContentType("xml", "json")]
        public IActionResult Get([FromQuery] RequestedTimeRange requestedTimeRange)
        {
            var rate = _rateProvider.GetRate(requestedTimeRange);
            if (rate == null)
                return NotFound();
            else
                return _responseFormatter.Format(new RateInfo() { Rate = rate.Value }, HttpContext.Request.Headers);
        }
    }

    public class RateInfo
    {
        public int Rate {get; set;}
    }
}
