using Microsoft.AspNetCore.Mvc;
using System.Xml.Serialization;
using spothero_project.Utils;

namespace spothero_project.CustomActionResults
{
    public class XmlContentResult : ContentResult
    {
        public XmlContentResult(object content)
        {
            Content = XmlUtils.SerializeToXml(content);
            ContentType = "application/xml";
        }
    }
}