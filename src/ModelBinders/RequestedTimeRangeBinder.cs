using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;
using spothero_project.Utils;
using spothero_project.Models;
using System.Linq;

namespace spothero_project.ModelBinders
{
    public class RequestedTimeRangeBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var dateResults = RequestedTimeRange.DesiredParameterNames
                .Select(n => new { ParamName = n, Result = bindingContext.ValueProvider.GetValue(n) })
                .ToList();
            
            if (dateResults.Any(dr => dr.Result == ValueProviderResult.None))
                return Task.CompletedTask;

            try
            {
                var dates = dateResults
                    .Select(x => GetDateTime(x.ParamName, x.Result))
                    .ToList();
                
                bindingContext.Result = ModelBindingResult.Success(DateTimeUtils.CreateRequestedTimeRange(dates[0], dates[1]));
            }
            catch (Exception ex)
            {
                if (ex is DateTimeConvertionException dateTimeConversionException)
                    bindingContext.ModelState.TryAddModelError(dateTimeConversionException.ParamName, dateTimeConversionException.Message);
                else
                    bindingContext.ModelState.TryAddModelError(string.Join(" and ", RequestedTimeRange.DesiredParameterNames), ex.Message);
            }

            return Task.CompletedTask;
        }

        private DateTime GetDateTime(string paramName, ValueProviderResult r)
        {
            try { return DateTimeUtils.DateTimeFromIsoString(r.FirstValue); }
            catch (Exception ex) { throw new DateTimeConvertionException(paramName, ex); }
        }
    }

    public class DateTimeConvertionException : Exception
    {
        public string ParamName {get; private set;}

        public DateTimeConvertionException(string paramName, Exception conversionException)
            : base($"Could not convert {paramName} to a valid date and time")
        {
            ParamName = paramName;
        }
    }
}