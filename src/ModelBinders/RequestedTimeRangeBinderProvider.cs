using Microsoft.AspNetCore.Mvc.ModelBinding;
using spothero_project.Models;

namespace spothero_project.ModelBinders
{
    public class RequestedTimeRangeBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context?.Metadata?.ModelType == typeof(RequestedTimeRange))
                return new RequestedTimeRangeBinder();
                
            return null;
        }
    }
}