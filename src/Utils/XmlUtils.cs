using System.IO;
using System.Xml.Serialization;

namespace spothero_project.Utils
{
    public class XmlUtils
    {
        public static string SerializeToXml(object content)
        {
            var serializer = new XmlSerializer(content.GetType());
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, content);
                return writer.ToString();
            }
        }
    }
}