using System;
using System.Globalization;
using spothero_project.Models;

namespace spothero_project.Utils
{
    public static class DateTimeUtils
    {
        public static DateTime DateTimeFromIsoString(string isoFormattedDateTime)
            => DateTime.Parse(isoFormattedDateTime, null, DateTimeStyles.RoundtripKind);

        public static RequestedTimeRange CreateRequestedTimeRange(DateTime startDate, DateTime endDate)
        {
            if (startDate.Date != endDate.Date)
                throw new InvalidOperationException($"The two dates that specify the time range must occur on the same calendar day. The first date is on calendar day {startDate.Date.ToShortDateString()} and the second date is on calendar day {endDate.Date.ToShortDateString()}.");
            if (startDate.TimeOfDay >= endDate.TimeOfDay)
                throw new InvalidOperationException($"The start time must occur before the end time. The start time is {startDate.TimeOfDay} and the end time is {endDate.TimeOfDay}.");
            
            return new RequestedTimeRange() { Day = startDate.DayOfWeek, Hours = new HourRange() { StartHour = startDate.Hour, EndHour = endDate.Hour }};
        }
    }
}