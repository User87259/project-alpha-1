using System.IO;
using Microsoft.Extensions.FileProviders;

namespace spothero_project.Utils
{
    public static class FileProviderUtils
    {
        public static string GetFileContents(IFileProvider fileProvider, string filePath)
        {
            var fileInfo = fileProvider.GetFileInfo(filePath);
            using (var stream = fileInfo.CreateReadStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}