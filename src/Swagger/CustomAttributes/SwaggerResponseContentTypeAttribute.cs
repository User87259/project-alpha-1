using System;

namespace spothero_project.Swagger.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class SwaggerResponseContentTypeAttribute : Attribute
    {
        public SwaggerResponseContentTypeAttribute(params string[] responseTypes)
        {
            ResponseTypes = responseTypes;
        }

        public string[] ResponseTypes { get; private set; }
    }
}