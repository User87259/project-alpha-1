using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Linq;
using spothero_project.Models;
using System;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace spothero_project.Swagger.OperationFilters
{
    public class RequestedTimeRangeFilter : IOperationFilter
    {
        private static readonly List<string> _requestedTimeRangeParams;

        static RequestedTimeRangeFilter()
        {
            //Hack - Unfortunately the asp.net mvc api description will yield a list of properties for a poco
            //rather than the poco itself (unless there are special circumstances such as binding with [FromHeader]).
            //Instead of fixing this issue just use a quick hack to get around it.
            _requestedTimeRangeParams = new List<string>()
            {
                nameof(RequestedTimeRange.Day),
                $"{nameof(RequestedTimeRange.Hours)}.{nameof(HourRange.StartHour)}",
                $"{nameof(RequestedTimeRange.Hours)}.{nameof(HourRange.EndHour)}"
            };
        }

        public void Apply(Operation operation, OperationFilterContext context)
        {
            var props = context.ApiDescription.ParameterDescriptions
                .Where(pd => _requestedTimeRangeParams.Contains(pd.Name))
                .ToList();

            if (props.Count == _requestedTimeRangeParams.Count)
            {
                props.ForEach(prop => operation.Parameters.Remove(operation.Parameters.Single(p => p.Name == prop.Name)));
                RequestedTimeRange.DesiredParameterNames.ForEach(name => operation.Parameters.Add(CreateDateTimeParameter(name, props.First())));
            }
        }

        private NonBodyParameter CreateDateTimeParameter(string name, ApiParameterDescription propDescription)
            => new NonBodyParameter() { Name = name, In = propDescription.Source.Id.ToLower(), Type = typeof(DateTime).Name.ToLower() };
    }
}