using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Linq;
using spothero_project.Models;
using System;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using spothero_project.Swagger.CustomAttributes;

namespace spothero_project.Swagger.OperationFilters
{
    public class BadRequestOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            //TO DO - The operation that is added here for a bad request will not show an example model in the swagger ui.
            //At least part of this is added by setting the Response.Schema property but unfortunately no schema is generated
            //for type ModelStateDictionary, this would have to be figured out to add this extra bit of documentation.
            if (context.ApiDescription.HttpMethod == "GET" && context.ApiDescription.ParameterDescriptions.Any())
                operation.Responses["400"] = new Response() { Description = "Bad Request" };
        }
    }
}