using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Linq;
using spothero_project.Models;
using System;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using spothero_project.Swagger.CustomAttributes;

namespace spothero_project.Swagger.OperationFilters
{
    public class ContentTypeOperationFilter : IOperationFilter
    {
        private static readonly Dictionary<string, List<string>> _nameToContentTypes;

        static ContentTypeOperationFilter()
        {
            _nameToContentTypes = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
            _nameToContentTypes["xml"] = new List<string>() { "application/xml", "text/xml" };
            _nameToContentTypes["json"] = new List<string>() { "application/json", "text/json" };
        }

        public void Apply(Operation operation, OperationFilterContext context)
        {
            //The xml formatter is added globally so it should apply to all GET endpoints
            if (context.ApiDescription.HttpMethod == "GET")
            {
                var requestAttributes = context.ApiDescription.ActionAttributes().OfType<SwaggerResponseContentTypeAttribute>().FirstOrDefault();

                if (requestAttributes != null)
                {
                    operation.Produces = requestAttributes.ResponseTypes
                        .OrderBy(x => x)
                        .SelectMany(rt => _nameToContentTypes[rt])
                        .ToList();
                }
            }
        }
    }
}