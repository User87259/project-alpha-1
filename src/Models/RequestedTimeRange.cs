using System;
using System.Collections.Generic;

namespace spothero_project.Models
{
    public class RequestedTimeRange
    {
        public DayOfWeek Day {get; set;}
        public HourRange Hours {get; set;}

        public static readonly List<string> DesiredParameterNames = new List<string>() { "startDate", "endDate" };
    }
}