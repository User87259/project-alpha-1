using System.Collections.Generic;
using System.Linq;

namespace spothero_project.Models
{
    public class RatesModel : IRatesModel
    {
        public List<RateModel> Rates {get; private set;}

        public RatesModel(List<RateModel> rates)
        {
            Rates = rates;
        }
    }
}