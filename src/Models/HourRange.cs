namespace spothero_project.Models
{
    public struct HourRange
    {
        public int StartHour {get; set;}
        public int EndHour {get; set;}
    }
}