using System.Collections.Generic;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace spothero_project.Models
{
    public class RateModel
    {
        public readonly List<DayOfWeek> Days;
        public readonly HourRange Hours;
        public readonly int Price;

        public RateModel(List<DayOfWeek> days, HourRange hours, int price)
        {
            Days = days;
            Hours = hours;
            Price = price;
        }
    }
}