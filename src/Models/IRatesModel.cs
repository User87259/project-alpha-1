using System.Collections.Generic;

namespace spothero_project.Models
{
    public interface IRatesModel
    {
        List<RateModel> Rates {get;}
    }
}