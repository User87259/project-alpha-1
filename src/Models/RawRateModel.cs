namespace spothero_project.Models
{
    public class RawRateModel
    {
        public string Days {get; set;}
        public string Times {get; set;}
        public int Price {get; set;}
    }
}