FROM ubuntu:16.04

# Install the .Net Core 2.0.2 Sdk
RUN apt-get update \
    && apt-get install -y curl \
    && curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg \
    && mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg \
    && echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-xenial-prod xenial main" > /etc/apt/sources.list.d/dotnetdev.list

RUN apt-get install -y apt-transport-https \ 
    && apt-get update \
    && apt-get install -y dotnet-sdk-2.0.2

ENV ASPNETCORE_URLS=http://+:80 ASPNETCORE_ENVIRONMENT=Production

# Download the app
RUN apt-get install -y git

RUN mkdir /home/code \
    && cd /home/code \
    && git clone https://User87259@bitbucket.org/User87259/project-alpha-1.git

CMD dotnet run --project /home/code/project-alpha-1/src