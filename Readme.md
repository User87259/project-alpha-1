# Project Alpha 1

--------------

The Dockerfile in the root of the project can be used to run the application. It can be built and run locally with

```docker
docker build . -t project_alpha_1
docker run -p 5000:80 -d --name projA1_container project_alpha_1
```

The container could be run in an environment such as Kubernetes very simply if desired. The swagger api view is available at the endpoint "/swagger" (which is "http://localhost:5000/swagger" when running locally with the docker commands given above). Metrics are gathered using Azure Application Insights so if the application is being run in a cloud environment make sure that the container can submit application insight data to Azure. Application insights should be sending its data over http or https so as long as the container has the ability to make http and https network requests the metrics collection should work.