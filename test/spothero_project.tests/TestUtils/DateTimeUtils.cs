using System;

namespace spothero_project.tests.TestUtils
{
    public static class DateTimeTestUtils
    {
        public static string DateTimeToIsoString(DateTime dateTime)
            => dateTime.ToString("s");
    }
}