using System;
using Xunit;
using spothero_project.Services;
using spothero_project.Models;
using Autofac.Extras.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace spothero_project.tests
{
    public class RateProviderTests
    {
        private RateModel _testRateModel =
            new RateModel(new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Tuesday }, new HourRange() { StartHour = 5, EndHour = 12 }, 375);

        [Fact]
        public void Returns_Correct_Rate_When_Request_Is_Completely_Inside_Existing_Range()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var rate = CallGetRate(mock, _testRateModel,
                    new RequestedTimeRange() { Day = DayOfWeek.Monday, Hours = new HourRange() { StartHour = 6, EndHour = 8 } });

                Assert.Equal(375, rate.Value);
            }
        }

        [Fact]
        public void Returns_Null_When_The_Request_Starts_In_An_Existing_Range_But_Ends_Outside_Of_It()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var rate = CallGetRate(mock, _testRateModel,
                    new RequestedTimeRange() { Day = DayOfWeek.Monday, Hours = new HourRange() { StartHour = 6, EndHour = 14 } });

                Assert.Null(rate);
            }
        }

        [Fact]
        public void Returns_Null_When_The_Request_Ends_In_An_Existing_Range_But_Starts_Outside_Of_It()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var rate = CallGetRate(mock, _testRateModel,
                    new RequestedTimeRange() { Day = DayOfWeek.Monday, Hours = new HourRange() { StartHour = 4, EndHour = 8 } });

                Assert.Null(rate);
            }
        }

        [Fact]
        public void Returns_Null_When_The_Request_Starts_At_The_Start_Of_An_Existing_Range_And_Ends_Inside_Of_It()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var rate = CallGetRate(mock, _testRateModel,
                    new RequestedTimeRange() { Day = DayOfWeek.Monday, Hours = new HourRange() { StartHour = 5, EndHour = 10 } });

                Assert.Null(rate);
            }
        }

        [Fact]
        public void Returns_Null_When_The_Request_Ends_At_The_End_Of_An_Existing_Range_And_Starts_Inside_Of_It()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var rate = CallGetRate(mock, _testRateModel,
                    new RequestedTimeRange() { Day = DayOfWeek.Monday, Hours = new HourRange() { StartHour = 6, EndHour = 12 } });

                Assert.Null(rate);
            }
        }

        private int? CallGetRate(AutoMock mock, RateModel rateModel, RequestedTimeRange requestedTimeRange)
        {
            mock.Mock<IRatesModel>()
                    .Setup(m => m.Rates)
                    .Returns(new List<RateModel>() { rateModel });

            var rateProvider = mock.Create<RateProvider>();
            return rateProvider.GetRate(requestedTimeRange);
        }
    }
}