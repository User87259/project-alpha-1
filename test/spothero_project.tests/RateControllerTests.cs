using System;
using Xunit;
using spothero_project.Controllers;
using spothero_project;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.WebUtilities;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.IO;
using Dazinator.AspNet.Extensions.FileProviders;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;

namespace spothero_project.tests
{
    public class RateControllerTests
    {
        [Fact]
        [Trait("Category", "Integration")]
        public async Task RatesController_Returns_Correct_Rate_For_Valid_Query_Accepting_A_Json_Response_In_Integration_Test()
        {
            var response = await CallApiRateEndpoint("2015-07-01T07:00:00Z", "2015-07-01T12:00:00Z", "application/json");
            Assert.Equal(response.StatusCode, HttpStatusCode.OK);

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<RateInfo>(content);
            Assert.Equal(1500, result.Rate);
        }

        [Fact]
        [Trait("Category", "Integration")]
        public async Task RatesController_Returns_Correct_Rate_For_Valid_Query_Accepting_An_Xml_Response_In_Integration_Test()
        {
            var response = await CallApiRateEndpoint("2015-07-01T07:00:00Z", "2015-07-01T12:00:00Z", "application/xml");
            Assert.Equal(response.StatusCode, HttpStatusCode.OK);

            //Hack - Extra StringReader used here to get around "There is no unicode byte order mark" error when deserializing the xml
            //see http://www.developersalley.com/blog/post/2011/05/24/How-To-Fix-There-Is-No-Unicode-Byte-Order-Mark-Error-C.aspx
            using (var reader = new StringReader(await response.Content.ReadAsStringAsync()))
            {
                var xmlSerializer = new XmlSerializer(typeof(RateInfo));
                var result = (RateInfo)xmlSerializer.Deserialize(reader);

                Assert.Equal(1500, result.Rate);
            }
        }

        [Fact]
        [Trait("Category", "Integration")]
        public async Task RatesController_Returns_BadRequest_For_Invalid_Query_In_Integration_Test()
        {
            var response = await CallApiRateEndpoint("adfece", "nkklh", "application/json");
            Assert.Equal(response.StatusCode, HttpStatusCode.BadRequest);
        }

        [Fact]
        [Trait("Category", "Integration")]
        public async Task RatesController_Returns_NotFound_For_Valid_Query_That_Does_Not_Match_Any_Interval_In_Integration_Test()
        {
            var response = await CallApiRateEndpoint("2015-07-01T01:00:00Z", "2015-07-01T20:00:00Z", "application/json");
            Assert.Equal(response.StatusCode, HttpStatusCode.NotFound);
        }

        private async Task<HttpResponseMessage> CallApiRateEndpoint(string startDateIsoString, string endDateIsoString, string acceptMediaType)
        {
            var testServer = CreateTestServer(_ratesJson);
            var client = testServer.CreateClient();

            var requestMessage = CreateRequestMessage(startDateIsoString, endDateIsoString, acceptMediaType);
            return await client.SendAsync(requestMessage);
        }

        private TestServer CreateTestServer(string ratesJson)
        {
            var provider = new InMemoryFileProvider();
            provider.Directory.AddFile("DeploymentFiles/", new StringFileInfo(ratesJson, "rates.json"));

            var builder = new WebHostBuilder()
                .UseEnvironment(Startup.INTEGRATION_TESTING_ENV_NAME)
                .ConfigureServices(sc =>
                {
                    sc.AddTransient<IFileProvider>(sp => provider);
                })
                .UseStartup(typeof(Startup));

            return new TestServer(builder);
        }

        private string CreateRequestUrl(string startDateIsoString, string endDateIsoString)
        {
            var queryParams = new Dictionary<string, string>()
                { { "startDate", startDateIsoString }, { "endDate", endDateIsoString } };
            
            return QueryHelpers.AddQueryString("/api/Rate", queryParams);
        }

        private HttpRequestMessage CreateRequestMessage(string startDateIsoString, string endDateIsoString, string acceptMediaType)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, CreateRequestUrl(startDateIsoString, endDateIsoString));
            requestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(acceptMediaType));

            return requestMessage;
        }

        private string _ratesJson =
            @"{
                ""rates"": [
                {
                    ""days"": ""mon,tues,wed,thurs,fri"",
                    ""times"": ""0600-1800"",
                    ""price"": 1500
                }]
            }";
    }
}