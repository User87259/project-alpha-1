using System;
using Xunit;
using spothero_project.ModelBinders;
using Autofac.Extras.Moq;
using Moq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Threading.Tasks;
using System.Collections.Generic;
using spothero_project.Models;
using Microsoft.Extensions.Primitives;
using spothero_project.tests.TestUtils;

namespace spothero_project.tests
{
    public class RequestedTimeRangeBinderTests
    {
        [Fact]
        public void Created_A_RequestedTimeRange_When_Given_A_Valid_Set_Of_Inputs()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var startDate = DateTime.MinValue;
                var endDate = startDate.AddHours(1);

                var result = RunBindModelAsync(mock, new Dictionary<string, string>() { {"startDate", DateTimeTestUtils.DateTimeToIsoString(startDate)}, {"endDate", DateTimeTestUtils.DateTimeToIsoString(endDate)} });

                //Make sure that a model was set on the binding context
                Assert.NotNull(result);
                Assert.Equal(true, result.Value.IsModelSet);

                Assert.IsType<RequestedTimeRange>(result.Value.Model);
                var resultRange = (RequestedTimeRange)result.Value.Model;

                Assert.Equal(resultRange.Day, startDate.DayOfWeek);
                Assert.Equal(resultRange.Hours.StartHour, startDate.Hour);
                Assert.Equal(resultRange.Hours.EndHour, endDate.Hour);               
            }
        }

        [Fact]
        public void Unless_Both_Parameters_Are_Present_A_RequestedTimeRange_Is_Not_Created()
        {
            using (var mock = AutoMock.GetLoose())
            {
                Assert.Null(RunBindModelAsync(mock, new Dictionary<string, string>()));
            }
        }

        private ModelBindingResult? RunBindModelAsync(AutoMock mock, Dictionary<string, string> paramNameToValueCollection)
        {
                ConfigureModelBindingContextToProvideParameters(mock, paramNameToValueCollection);

                ModelBindingResult? result = null;
                mock.Mock<ModelBindingContext>()
                    .SetupSet(m => m.Result = It.IsAny<ModelBindingResult>())
                    .Callback<ModelBindingResult>(r => result = r);

                var binder = mock.Create<RequestedTimeRangeBinder>();
                binder.BindModelAsync(mock.Mock<ModelBindingContext>().Object);

                return result;
        }

        private void ConfigureModelBindingContextToProvideParameters(AutoMock mock, Dictionary<string, string> paramNameToValueCollection)
        {
            mock.Mock<IValueProvider>()
                .Setup(m => m.GetValue(It.IsAny<string>()))
                .Returns<string>(p =>
                {
                    if (paramNameToValueCollection.ContainsKey(p))
                        return new ValueProviderResult(new StringValues(paramNameToValueCollection[p]));
                    else
                        return ValueProviderResult.None;
                });

            mock.Mock<ModelBindingContext>()
                .Setup(m => m.ValueProvider)
                .Returns(mock.Mock<IValueProvider>().Object);
        }
    }
}