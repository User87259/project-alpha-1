using System;
using Xunit;
using spothero_project.Utils;

namespace spothero_project.tests
{
    public class DateTimeUtilsTests
    {
        [Fact]
        public void Correctly_Parses_Valid_ISO_DateTime_String()
        {
            var result = DateTimeUtils.DateTimeFromIsoString("2015-07-01T07:00:00Z");

            Assert.Equal(result.Day, 1);
            Assert.Equal(result.Month, 7);
            Assert.Equal(result.Year, 2015);

            Assert.Equal(result.Second, 0);
            Assert.Equal(result.Minute, 0);
            Assert.Equal(result.Hour, 7);
        }

        [Fact]
        public void Throws_Format_Exception_For_Invalid_ISO_DateTime_String()
            => Assert.Throws<FormatException>(() => DateTimeUtils.DateTimeFromIsoString("adsafomdsf"));

        [Fact]
        public void Creates_Requested_Time_Range_For_Two_Times_On_The_Same_Calendar_Day_That_Are_Ordered_Correctly()
        {
            var startDate = DateTime.MinValue;
            var endDate = startDate.AddHours(1);

            var result = DateTimeUtils.CreateRequestedTimeRange(startDate, endDate);
            
            Assert.Equal(result.Day, startDate.DayOfWeek);
            Assert.Equal(result.Hours.StartHour, startDate.Hour);
            Assert.Equal(result.Hours.EndHour, endDate.Hour);
        }

        [Fact]
        public void Creating_A_Requested_Time_Range_Throws_When_The_Start_And_End_Dates_Are_On_Two_Different_Calendar_Days()
        {
            var startDate = DateTime.MinValue;
            var endDate = startDate.AddDays(1);

            Assert.Throws<InvalidOperationException>(() => DateTimeUtils.CreateRequestedTimeRange(startDate, endDate));
        }

        [Fact]
        public void Creating_A_Requested_Time_Range_Throws_When_The_Start_And_End_Dates_Are_On_The_Same_Calendar_Day_But_The_Start_Time_Comes_After_The_End_Time()
        {
            var endDate = DateTime.MinValue;
            var startDate = endDate.AddHours(1);

            Assert.Throws<InvalidOperationException>(() => DateTimeUtils.CreateRequestedTimeRange(startDate, endDate));
        }

        [Fact]
        public void Creating_A_Requested_Time_Range_Throws_When_The_Start_And_End_Dates_Are_On_The_Same_Calendar_Day_But_The_Start_Time_Equals_The_End_Time()
            => Assert.Throws<InvalidOperationException>(() => DateTimeUtils.CreateRequestedTimeRange(DateTime.MinValue, DateTime.MinValue));
    }
}